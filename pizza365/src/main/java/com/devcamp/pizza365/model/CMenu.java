package com.devcamp.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class CMenu {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(name = "size")
  private String size;
  @Column(name = "duong_kinh")
  private String duongkinh;
  @Column(name = "suon_nuong")
  private int suonnuong;
  @Column(name = "salad")
  private String salad;
  @Column(name = "nuoc_ngot")
  private int nuocngot;
  @Column(name = "don_gia")
  private String dongia;

  public CMenu() {
  }

  public CMenu(long id, String size, String duongkinh, int suonnuong, String salad, int nuocngot, String dongia) {
    this.id = id;
    this.size = size;
    this.duongkinh = duongkinh;
    this.suonnuong = suonnuong;
    this.salad = salad;
    this.nuocngot = nuocngot;
    this.dongia = dongia;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getDuongkinh() {
    return duongkinh;
  }

  public void setDuongkinh(String duongkinh) {
    this.duongkinh = duongkinh;
  }

  public int getSuonnuong() {
    return suonnuong;
  }

  public void setSuonnuong(int suonnuong) {
    this.suonnuong = suonnuong;
  }

  public String getSalad() {
    return salad;
  }

  public void setSalad(String salad) {
    this.salad = salad;
  }

  public int getNuocngot() {
    return nuocngot;
  }

  public void setNuocngot(int nuocngot) {
    this.nuocngot = nuocngot;
  }

  public String getDongia() {
    return dongia;
  }

  public void setDongia(String dongia) {
    this.dongia = dongia;
  }

}
