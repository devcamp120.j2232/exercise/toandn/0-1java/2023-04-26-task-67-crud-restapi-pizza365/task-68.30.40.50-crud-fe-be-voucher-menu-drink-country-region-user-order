package com.devcamp.pizza365.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "drinks")
public class CDrink {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @NotNull(message = "Nhập mã nước uống")
  @Size(min = 1, message = "Mã nước uống có ít nhất 1 ký tự")
  @Column(name = "ma_nuoc_uong", unique = true)
  private String maNuocUong;

  @NotNull(message = "Nhập tên nước uống")
  @Size(min = 1, message = "Tên nước uống có ít nhất 1 ký tự")
  @Column(name = "ten_nuoc_uong")
  private String tenNuocUong;

  @NotNull(message = "Nhập đơn giá")
  @Range(min = 10000, max = 500000, message = "Nhập giá trị từ 10000 đến 500000")
  @Column(name = "gia_nuoc_uong")
  private int price;

  @Column(name = "ghi_chu")
  private String ghiChu;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ngay_tao", nullable = true, updatable = false)
  @CreatedDate
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date ngayTao;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ngay_cap_nhat", nullable = true)
  @LastModifiedDate
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date ngayCapNhat;

  public CDrink() {

  }

  public CDrink(int id,
      String maNuocUong,
      String tenNuocUong,
      int price,
      String ghiChu,
      Date ngayTao, Date ngayCapNhat) {
    this.id = id;
    this.maNuocUong = maNuocUong;
    this.tenNuocUong = tenNuocUong;
    this.price = price;
    this.ghiChu = ghiChu;
    this.ngayTao = ngayTao;
    this.ngayCapNhat = ngayCapNhat;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMaNuocUong() {
    return maNuocUong;
  }

  public void setMaNuocUong(String maNuocUong) {
    this.maNuocUong = maNuocUong;
  }

  public String getTenNuocUong() {
    return tenNuocUong;
  }

  public void setTenNuocUong(String tenNuocUong) {
    this.tenNuocUong = tenNuocUong;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getGhiChu() {
    return ghiChu;
  }

  public void setGhiChu(String ghiChu) {
    this.ghiChu = ghiChu;
  }

  public void setNgayTao(Date ngayTao) {
    this.ngayTao = ngayTao;
  }

  public Date getNgayTao() {
    return ngayTao;
  }

  public Date getNgayCapNhat() {
    return ngayCapNhat;
  }

  public void setNgayCapNhat(Date ngayCapNhat) {
    this.ngayCapNhat = ngayCapNhat;
  }

}
