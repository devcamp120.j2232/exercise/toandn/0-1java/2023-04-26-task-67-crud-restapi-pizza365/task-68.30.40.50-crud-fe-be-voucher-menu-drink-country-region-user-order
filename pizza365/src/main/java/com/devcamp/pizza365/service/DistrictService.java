package com.devcamp.pizza365.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.District;
import com.devcamp.pizza365.repository.IDistrictRepository;

@Service
public class DistrictService {
  @Autowired
  IDistrictRepository pIDistrictRepository;

  public ArrayList<District> getAllDistricts() {
    ArrayList<District> districts = new ArrayList<>();
    pIDistrictRepository.findAll().forEach(districts::add);
    return districts;
  }

}
