package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.service.CountryService;
import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/country")
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	CountryService countryService;

	@CrossOrigin
	@PostMapping("/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			return new ResponseEntity<>(countryService.createCountry(cCountry), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/count")
	public long countCountry() {
		return countryRepository.count();
	}

	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử
	 * dụng hàm existsById() của CountryRepository
	 * 
	 * @param id
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}

	/**
	 * Tìm country có chứa giá trị trong country code
	 * 
	 * @param code
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/containing-code/{code}")
	public List<CCountry> getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@CrossOrigin
	@GetMapping("/detail/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/all")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/country1")
	public CCountry getCountryByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
		return countryRepository.findByCountryCode(countryCode);
	}

	@CrossOrigin
	@GetMapping("/country2")
	public List<CCountry> getCountryByCountryName(@RequestParam(value = "countryName") String countryName) {
		return countryRepository.findByCountryName(countryName);
	}

	@CrossOrigin
	@GetMapping("/country3")
	public CCountry getCountryByRegionCode(@RequestParam(value = "regionCode") String regionCode) {
		return countryRepository.findByRegionsRegionCode(regionCode);
	}

	@CrossOrigin
	@GetMapping("/country4")
	public List<CCountry> getCountryByRegionName(@RequestParam(value = "regionName") String regionName) {
		return countryRepository.findByRegionsRegionName(regionName);
	}
}
