package com.devcamp.pizza365.repository;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.District;

public interface IDistrictRepository extends JpaRepository<District, Integer> {
  District findById(int Id);

  List<District> findByRegionId(int Id);
}
